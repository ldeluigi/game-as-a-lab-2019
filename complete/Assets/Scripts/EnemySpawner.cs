﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private float timeToNextSpawn;
    private int enemyCounter;

    [SerializeField]
    private GameObject enemyPrefab;
    [SerializeField]
    private float maxTimeBetweenSpawns = 4;
    [SerializeField]
    private float minTimeBetweenSpawns = 1;
    [Range(0, 1)]
    [SerializeField]
    private float decreaseRate = 0.95f;
    [SerializeField]
    private CharacterHealth playerHealth;

    private void Start()
    {
        playerHealth.Died += OnPlayerDeath;
    }

    private void OnPlayerDeath()
    {
        enabled = false;
    }

    private void Update()
    {
        timeToNextSpawn -= Time.deltaTime;
        if (timeToNextSpawn <= 0)
        {
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        enemyCounter++;
        timeToNextSpawn = Mathf.Lerp(minTimeBetweenSpawns, maxTimeBetweenSpawns, Mathf.Pow(decreaseRate, enemyCounter));
        Transform spawnPoint = transform.GetChild(Random.Range(0, transform.childCount));
        Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);
    }
}
